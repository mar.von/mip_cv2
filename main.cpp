#include "mbed.h"
#include <string>
#include <map>

#define LONG_SIGNAL 1
#define SHORT_SIGNAL 0.2
#define LONG_PAUSE 1
#define SHORT_PAUSE 0.2

std::map<char,std::string> CreateMorseCodeMap();
void LongBlick();
void ShortBlick();


DigitalOut myled(LED1);

std::map <char, std::string>  morseCode = CreateMorseCodeMap();
map<char, std::string>::iterator it; 
    
int main() {
    std::string name = "martin";  
    while(1)
    {
        for (int i = 0; i < name.length(); i++) 
        {
            it = morseCode.find(name[i]);
            if (it != morseCode.end())
            {
                for (int j = 0; j < it->second.length(); j++)
                {
                    if(it->second[j] == '.')
                    {
                        ShortBlick();
                    }else if(it->second[j] == '-')
                    {
                        LongBlick();
                    }
                }
                wait(LONG_PAUSE);
            }
        }
        wait(LONG_PAUSE*5);
    } 
}

std::map<char,std::string> CreateMorseCodeMap()
{
  map<char,std::string> m;
  m['a'] = ".-";
  m['b'] = "-...";
  m['c'] = "-.-.";
  m['d'] = "-..";
  m['e'] = ".";
  m['f'] = "..-.";  
  m['g'] = "--.";  
  m['h'] = "....";
  m['i'] = "..";
  m['j'] = ".---";
  m['k'] = "-.-.";
  m['l'] = ".-..";
  m['m'] = "--";
  m['n'] = "-.";
  m['o'] = "---";
  m['p'] = ".--.";
  m['q'] = "--.-";
  m['r'] = ".-."; 
  m['s'] = "..."; 
  m['t'] = "-";  
  m['u'] = "..-";
  m['v'] = "...-";
  m['w'] = ".--";
  m['x'] = "-..-";
  m['y'] = "-.--";
  m['z'] = "--..";  
  return m;
}

void LongBlick() {  
      myled = 1; // LED is ON
      wait(LONG_SIGNAL); // 200 ms
      myled = 0; // LED is OFF
      wait(SHORT_PAUSE); // 1 sec
}
void ShortBlick() {  
      myled = 1; // LED is ON
      wait(SHORT_SIGNAL); // 200 ms
      myled = 0; // LED is OFF
      wait(SHORT_PAUSE); // 1 sec
}
